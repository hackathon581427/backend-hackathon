describe('Search Endpoint', () => {
	it('should return trips for valid search parameters', () => {
		const departure = 'Paris'
		const arrival = 'Lyon'
		const date = '2024-07-05' // Replace with a valid date

		cy.request({
			method: 'GET',
			url: `/search/${departure}/${arrival}/${date}`,
		}).then((response) => {
			// Assertions
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.true
			expect(response.body.trips).to.be.an('array')
		})
	})

	it('should return an error for invalid search parameters', () => {
		const invalidDeparture = 'Paris'
		const invalidArrival = 'Lyon'
		const invalidDate = '2024-01-10'

		cy.request({
			method: 'GET',
			url: `/search/${invalidDeparture}/${invalidArrival}/${invalidDate}`,
		}).then((response) => {
			// Assertions
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.false
			expect(response.body.error).to.equal('No trip found')
		})
	})
})
